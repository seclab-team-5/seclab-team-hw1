# Seclab team hw1 report

## (2 pts) What is the usage of Vagrantfile in the project, and why?
The Vagrantfile specify how to set up a virtual machine to run the application. With this file, a virtual machine does not need to be configured manually. Instead, the user only needs to run ```vagrant up```. This ensures a consistent VM environment can be easily set up to run the application.

## (4 pts) Check the changes from Dockerfile to Dockerfile, describe at least four reasons about why did they make these changes.
- Change to alpine-based image to make the image smaller. 
- create user with uid 1001 and run the ```docker-entrypoint.sh``` with this user (instead of root) to comply with the principle of least privilege.
- Add --no-cache-dir option to pip install. No one else will install packages in this environment, so cache is disabled to save disk space.
- The to copy a directory into docker container, VOLUME command is unnecessary unless the change of files should be kept after the termination of container. Therefore the latter Dockerfile rewrites the directory copy procedure to make the code cleaner.


## (24 pts) Report of your deployment strategies. Describe your strategies in graphs or sentences clearly. Requirements:

### Strategy A: Cloud Storage deployment
#### How it works
- Wait for the build stage to complete
- After the build stage succeed, the whole repository is copied to a bucket on gcloud storage
- Instruct CI/CD to SSH into the deploy machine and run the following jobs:
    - Copy the repository from gcloud storage.
    - install docker & docker-compose
    - kill old tmux session to stop previous deployed service.
    - start a new tmux session and run ```docker-compose up```
- ctfd will be running on port 8000 within several minutes. 

##### requirements:
- Secure password.
    Our gitlab & google cloud passwords are secure :) This deployment strategy does not require additional passwords.
- Avoid hardcoding secrets.
    Gcloud credentials and storage bucket names are define in the CI/CD variables, and marked as protected so that they are only exported to protected branches.
- Secure communications between GitLab and your service (e.g. TLS).
    All communications are secured
    - between CI/CD & Gcloud storage: SCP
    - between Gcloud storage & deployment machine: SCP
    - between CI/CD & deployment machine: SSH
- Access Control of CD trigger.
    Set ```deploy-a``` as a protected branch. Only maintainers can push/merge into this branch or run CI/CD pipelines. 

#### How to initialize
1. Create an gooogle compute engine instance from gcloud console (Ubuntu 18.04LTS, n1-standard-1). Check Allow http and https traffic options in the firewall section.
2. Add the following to CI/CD variables: ```DEPLOY_INSTANCE_NAME_A=<instance-name>```, ```DEPLOY_PROJECT_ID_A=<project-id>```, ```DEPLOY_ZONE_A=<instance-zone>```, these variables are required for CI/CD to locate the deployment machine.
3. Run the pipeline by either pushing to branch ```deploy-a``` or manually via gitlab web interface.
4. Ctfd should be running on port 8000 within a few minutes after the pipeline passes.
**NOTE 1**: As ```docker-compose up``` is still running in a tmux session after the CI/CD pipeline exits, you may need to wait for another few minutes for ctfd to be ready. You may ssh into the machine manually and run ```sudo tmux a -t ctfd``` to monitor the progress.
**NOTE 2**: You may encounter the error message```tmux no server running on ...``` in the CI/CD log. This is ok, it just means that there are no previous tmux sessions to kill. ctfd should still be deployed successfully.



### Strategy B: Git pull and upgrade code on server
#### How it works
* set private ssh key for gitlab CI runner
* ssh to the instance
* pull the latest source code
* kill the original process
* run the new deployed version on the background using ``screen``.
##### requirements:
- Secure password.
    Our gitlab & google cloud passwords are secure :) This deployment strategy does not require additional passwords.
- Avoid hardcoding secrets.
    All ssh related variable are define in the CI/CD variables, and marked as protected so that they are only exported to protected branches.
- Secure communications between GitLab and your service (e.g. TLS).
    All communication are secured
    - between CI/CD & deployment machine: SSH
- Access Control of CD trigger.
    Set ```deploy-b``` as a protected branch. Only maintainers can push/merge into this branch or run CI/CD pipelines. 
#### How to initialize
* Create a GCP Compute engine instance(Debian GNU/Linux 9) with allowence of HTTP and HTTPS traffic
* Create ssh key with no passphrase on a client machine
```
ssh-keygen -f deploy_key -N ""
```
* Add ``deploy_key.pub`` to the compute engine instance(edit/ssh keys/show and edit/save)
![](https://i.imgur.com/Xsu8rSL.png)
* Set the CI/CD variable, including
    * DEPLOY_B_IP: ip address of compute engine instance
    * DEPLOY_B_USER: user of key
    ![](https://i.imgur.com/HbqTYh4.png)
    * USER_SSH_PRIVATE_KEY: ``deploy_key`` with base64 encoding([ref](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/14434#note_36717658))
    ```
    cat deploy_key | base64 -w0
    ```

* ssh to the compute engine instance and config
```
ssh ${DEPLOY_B_USER}@${DEPLOY_B_IP} -i deploy_key
```
* On the compute engine instance
```
sudo apt update
sudo apt-get install git
git clone https://gitlab.com/seclab-team-5/seclab-team-hw1.git
sudo apt-get install python3-pip
sudo pip3 install -r requirements.txt
```
